#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint native_webview.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'native_webview'
  s.version          = '3.5.0'
  s.summary          = 'A Flutter plugin that provides a WebView widget on Android and iOS.'
  s.description      = <<-DESC
A Flutter plugin that provides a WebView widget on Android and iOS.
                       DESC
  s.homepage         = 'http://www.alc.ca'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Atlantic Lottery Corporation' => 'info@alc.ca' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.platform = :ios, '11.0'

  # Flutter.framework does not contain a i386 slice. Only x86_64 simulators are supported.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'VALID_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
  s.swift_version = '5.0'
  s.resource_bundles = {'native_webview_privacy' => ['PrivacyInfo.xcprivacy']}
end
