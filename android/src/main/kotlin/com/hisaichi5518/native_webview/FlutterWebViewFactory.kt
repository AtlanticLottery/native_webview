package com.hisaichi5518.native_webview

import android.content.Context
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.StandardMessageCodec
import io.flutter.plugin.platform.PlatformView
import io.flutter.plugin.platform.PlatformViewFactory

class FlutterWebViewFactory(
    private val messenger: BinaryMessenger
) : PlatformViewFactory(StandardMessageCodec.INSTANCE) {
    override fun create(context: Context, viewId: Int, args: Any?): PlatformView {
        val channel = MethodChannel(messenger, "com.hisaichi5518/native_webview_$viewId")
        val alcChannel = MethodChannel(messenger,"native_webview_console")

        return FlutterWebViewController(
            context,
            channel,
            alcChannel,
            args as Map<String, Any>
        )
    }
}