plugins {
    id("com.android.library")
    id("kotlin-android")
}

group = "com.hisaichi5518.native_webview"
version = "3.6.0"

android {
    namespace = "com.hisaichi5518.native_webview"
    compileSdk = flutter.compileSdkVersion
    ndkVersion = flutter.ndkVersion

    sourceSets {
        val main by getting
        main.java.srcDirs("src/main/kotlin")
    }

    defaultConfig {
        minSdk = 24
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_17.toString()
    }

    lint {
        disable += listOf(
            "InvalidPackage",
        )
    }

    dependencies {
        implementation("androidx.appcompat:appcompat:1.7.0")
    }
}

dependencies {
    implementation("androidx.webkit:webkit:1.2.0")
}
